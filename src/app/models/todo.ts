export class Todo {

  id: number;
  description: string;
  status: string;
  file: string;
}
