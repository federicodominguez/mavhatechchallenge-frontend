import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  todos: Todo[];
  todo: Todo;
  filter = false;
  id = '';
  status = '';
  description = '';
  url = 'http://localhost:8080/upload-dir/';
  errores: string[];

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.todoService.getTodos().subscribe(
      todos => {
        this.todos = todos
      },
      err => {
        this.errores = err.error.errors as string[];
        console.log(err.status);
        console.log(err.error.errors);
      }
    );
  }

  filterClick(){
      this.todoService.getFilteredTodos(this.id,this.description,this.status).subscribe(
        todos => {
          this.todos = todos
        },
        err => {
          this.errores = err.error.errors as string[];
          console.log(err.status);
          console.log(err.error.errors);
        }

      );
      this.filter = true;
  }

  clearFilter(){
    this.id = '';
    this.description = '';
    this.status = '';
    this.todoService.getTodos().subscribe(
      todos => {
        this.todos = todos
      },
      err => {
        this.errores = err.error.errors as string[];
        console.log(err.status);
        console.log(err.error.errors);
      }

    );
    this.filter = false;
  }

  resolved(index: number){
    this.todo = this.todos.find(item => item.id === index);
    this.todoService.updateTodoStatus(index).subscribe(
      response => {
        this.todo.status = 'DONE';
      err => {
        this.errores = err.error.errors as string[];
        console.log(err.status);
        console.log(err.error.errors);
      }
      });
  }

}
