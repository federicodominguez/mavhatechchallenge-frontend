import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  private title: string = "Create To do";
  private todo: Todo = new Todo();
  errores: string[];
  private file: File;

  constructor(private todoService: TodoService,
  private router: Router) { }

  ngOnInit() {
  }

  create(): void{
    let formData = new FormData();
    formData.append("description",this.todo.description);
    formData.append("file",this.file);
    this.todoService.create(formData).subscribe(
      response => { this.router.navigate(['/home']);
      swal('New todo', 'to do created successfully', 'success');
      },
    err => {
      this.errores = err.error.errors as string[];
      console.log(err.status);
      console.log(err.error.errors);
    }
  );
  }

  chooseFile(event){
    this.file = event.target.files[0];
    if(this.file.type.indexOf('image') < 0){
      swal('Error when choosing image','Image type is not supported','error');
      this.file = null;
    }
  }
}
