import { Injectable } from '@angular/core';
import { Todo } from 'src/app/models/todo';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  [x: string]: any;

  private readonly URL_API = 'http://localhost:8080/todo';
  private readonly QUERY_PARAM_ID = 'id';
  private readonly QUERY_PARAM_DESCRIPTION = 'description';
  private readonly QUERY_PARAM_STATUS = 'status';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http:HttpClient) { }

  getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.URL_API).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal('error returning to do', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  create(todo: FormData):Observable<FormData>{
    return this.http.post<FormData>(this.URL_API,todo).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal('error creating to do', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  updateTodoStatus(id:number):Observable<Todo>{
    return this.http.put<Todo>(`${this.URL_API}/${id}`, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal('error when editing to do', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }

  getFilteredTodos(id: string, description: string, status: string):Observable<Todo[]>{
    let params = new HttpParams();
    if (id != null && id !== '') {
      params = params.append(this.QUERY_PARAM_ID, id);
    }
    if (description != null && description !== '') {
      params = params.append(this.QUERY_PARAM_DESCRIPTION, description);
    }
    if (status != null && status !== '') {
    params = params.append(this.QUERY_PARAM_STATUS, status);
    }
    return this.http.get<Todo[]>(this.URL_API, {params}).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal('error filtering to do', e.error.mensaje,'error');
        return throwError(e);
      })
    );
  }


}
